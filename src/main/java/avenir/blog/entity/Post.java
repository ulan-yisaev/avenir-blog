package avenir.blog.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "posts")
@Getter
@Setter
public class Post extends BaseEntity {

    @Size(max = 200)
    private String title;

    private String content;

    private Timestamp created;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User author;

    private long views;
}
