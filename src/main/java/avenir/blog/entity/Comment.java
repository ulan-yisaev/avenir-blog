package avenir.blog.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "posts")
@Getter
@Setter
public class Comment extends BaseEntity {

    private Long idPost;

    private String content;

    private Timestamp created;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User author;

}
